package main

import (
	"fmt"
	"os"

	"golang.org/x/crypto/bcrypt"
)

func main() {
	pass := []byte(os.Args[1])
	hash := os.Args[2]

	if hash != "" {
		fmt.Printf("hash = %s\n", hash)
		if err := bcrypt.CompareHashAndPassword([]byte(hash), pass); err != nil {
			fmt.Printf("Password does not match hash: %s\n", err.Error())
			os.Exit(1)
		} else {
			fmt.Println("Password matches hash")
			os.Exit(0)

		}
	}
	bs, err := bcrypt.GenerateFromPassword(pass, bcrypt.DefaultCost)
	if err != nil {
		panic(fmt.Sprintf("Error generating hash from password: %s", err.Error()))
	}
	fmt.Printf("Hashed Password: %s\n", bs)
}
